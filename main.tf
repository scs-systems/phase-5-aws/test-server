// RJ

// backend.tf
// https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html
terraform {
  backend "http" {
  }
}

//connections.tf
provider "aws" {
  region = "eu-west-2"
}

//variables.tf
variable "ami_id" {
  default     = "ami-035cecbff25e0d91e" // Redhat 9 Stream
  description = "AMI ID."
}

variable "vpc_name" {
  default     = "test-vpc"
  description = "VPC Name"
}

variable "ami_name" {
  default     = "Redhat 9 release"
  description = "AMI Name"
}

variable "ami_key_pair_name" {
  default     = "richjam"
  description = "EC2 Key pair name"
}

variable "ssh_user" {default     = "ec2-user"}

variable "ssh_key" {default     = "junk"}

variable "payload" {
  default = "https://gitlab.com/scs-systems/phase-5-aws/infra/ansible-test-server.git"
  description = "https url of payload git repo"
}


//network.tf
resource "aws_vpc" "test-env" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name = "test-env"
  }
}

//subnets.tf
resource "aws_subnet" "subnet-uno" {
  cidr_block        = "${cidrsubnet(aws_vpc.test-env.cidr_block, 3, 1)}"
  vpc_id            = "${aws_vpc.test-env.id}"
  availability_zone = "eu-west-2a"
}

// security.tf


resource "aws_security_group" "ingress-all-test" {
name   = "allow-all-sg"
vpc_id = "${aws_vpc.test-env.id}"
ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }
// Terraform removes the default rule
  egress {
   from_port   = 0
   to_port     = 0
   protocol    = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }
}

//gateways.tf
resource "aws_internet_gateway" "test-env-gw" {
  vpc_id = "${aws_vpc.test-env.id}"
  tags =  {
    Name = "test-env-gw"
  }
}

//subnets.tf
resource "aws_route_table" "route-table-test-env" {
  vpc_id = "${aws_vpc.test-env.id}"
route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.test-env-gw.id}"
  }
  tags = {
    Name = "test-env-route-table"
  }
}

resource "aws_route_table_association" "subnet-association" {
  subnet_id      = "${aws_subnet.subnet-uno.id}"
  route_table_id = "${aws_route_table.route-table-test-env.id}"
}

//servers.tf
// https://discuss.hashicorp.com/t/terraform-wants-to-replace-my-aws-instance-with-no-modifications/12882
// https://stackoverflow.com/questions/51496944/terraform-forces-new-ec2-resource-creation-on-plan-apply-regarding-existing-secu
// Use of "security_groups" was forcing EC2 rebuild every time - fix is to use vpc_security_group_ids instead
resource "aws_instance" "test-ec2-instance" {
  ami             = "${var.ami_id}"
  instance_type   = "t2.micro"
  key_name        = "${var.ami_key_pair_name}"
  vpc_security_group_ids = ["${aws_security_group.ingress-all-test.id}"]
  tags = {
    Name = "${var.ami_name}"
  }
  subnet_id = "${aws_subnet.subnet-uno.id}" 
}

resource "aws_eip" "ip-test-env" {
  instance = "${aws_instance.test-ec2-instance.id}"
  domain      = "vpc"
}


resource "null_resource" "ssh_target" {
  // depends_on = [aws_eip.ip-test-env]
  connection {
    type        = "ssh"
    user        = var.ssh_user
    host        = aws_eip.ip-test-env.public_ip
    private_key = var.ssh_key
  }

  provisioner "remote-exec" {
    inline = [
      "sudo dnf makecache",
      "sudo dnf install git -y",
      "git clone ${var.payload} /tmp/payload",
      "echo \"${var.ssh_key}\" > /home/ec2-user/.ssh/id_rsa",
      "chmod 600 /home/ec2-user/.ssh/id_rsa",
      "sudo sh /tmp/payload/payload.sh >/tmp/payload.log 2>&1",
    ]
  }
}


output "aws_eip_public_ip" {
  description = "Public IP of aws_eip"
  value       = aws_eip.ip-test-env.public_ip
}
