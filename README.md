## Name
Test server 1/2/2024.
Retest 12/9/2024-3

## Description
So - I just wanted to use Terraform to:
* create an EC2 server
* Deliver my ssh private-key to the server - so it will be able to SSH to other servers I create
* Deliver a "payload" and run a script to install it

## Usage
Need to configured these CICD Variables:
* TF_VAR_ssh_key (Your private SSH KEY - to be installed on target server )
Annoying - cant mask it - see: https://gitlab.com/gitlab-org/gitlab/-/issues/220187
Code doesnt echo the key to logs
* AWS_ACCESS_KEY_ID (AWS access)
* AWS_SECRET_ACCESS_KEY (AWS access)

# Pipeline
* Creates EC2 - and all other required AWS components from VPN down
* Delivers and installs private key to ec2-user user ID
* Pulls a "payload" from a (variable defined) separate reporitory and puts in /tmp/payload
* Expects to find a "payload.sh" in in the payload - and runs it.
* Post run - payload.log is put in /tmp


## Authors and acknowledgment
https://medium.com/@hmalgewatta/setting-up-an-aws-ec2-instance-with-ssh-access-using-terraform-c336c812322f

## License
For open source projects, say how it is licensed.

## Project status
Works !
Payload is extracted from a separate project- ie: configurable
